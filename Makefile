all: tgalib.o xinit.o editor.o xstoneage.o link

xinit.o: xinit.c
	gcc -g -c xinit.c -I/usr/X11R6/include
tgalib.o: tgalib.c
	gcc -g -c tgalib.c -I/usr/X11R6/include
editor.o: editor.c
	gcc -g -c editor.c -I/usr/X11R6/include
xstoneage.o: xstoneage.c
	gcc -g -c xstoneage.c -I/usr/X11R6/include

link: xstoneage.o tgalib.o xinit.o
	gcc tgalib.o xinit.o editor.o xstoneage.o -L/usr/X11R6/lib -lX11 -lm -lz -o xstoneage

clean: 
	rm xstoneage.o tgalib.o xinit.o editor.o xstoneage stoneage.pak 2>/dev/null || true