/* quick and dirty utility to create gzipped stoneage.pak */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <zlib.h>

typedef unsigned char byte;

gzFile pakfile;

int loadmap(char* filename)
{
    int f,i;
    unsigned char buffer[768];
    printf("MAP: %s... ",filename);
    f=open(filename,O_RDONLY);
    i=read(f,&buffer,768);
    close(f);
    gzwrite(pakfile,&buffer,i);
    printf("OK\n");
}

int loadtga(char* filename)
{
    FILE *f;
    unsigned char header[18];
    unsigned char c,c2,*d;
    int i,j;
    int picx,picy;
    long newpixval;
    
    printf("PIC: %s... ",filename);
    
    //read tga file
    if((f=fopen(filename,"r"))){
        for(i=0;i<18;i++){header[i]=fgetc(f);};
        gzwrite(pakfile,&header,18);
        if((header[1]!=1 && header[1]!=0) || //color map type
           (header[2]!=1 && header[2]!=9) || //raw or RLE color mapped image
            header[7]!=24 ||                 //number of bits/color (8*3)
            header[16]!=8){                  //bits per pixel
                fclose(f);
                printf("Unsupported tga format.\n");
                return(1);
        }
        picx=header[12]+header[13]*256;
        picy=header[14]+header[15]*256;
         
        for(i=0;i<header[0];i++){ c=fgetc(f); gzwrite(pakfile,&c,1); }  //file info
        
        //read palette
        for(i=0;i<header[5]+header[6]*256;i++){
          j=header[3]+header[4]*256+i;
          j=j*3;
          c=fgetc(f); gzwrite(pakfile,&c,1);
          c=fgetc(f); gzwrite(pakfile,&c,1);
          c=fgetc(f); gzwrite(pakfile,&c,1);
        }
         
         if(header[2]==1){
            //raw image
            for(i=0;i<picy*picx;i++){
                c=fgetc(f); gzwrite(pakfile,&c,1);
            }
         } else {
            //rle compressed
            i=0;
            while(i<picy*picx){
                c2=fgetc(f); gzwrite(pakfile,&c2,1);
                if(c2 & 0x80){
                    //compressed package
                    c2&=0x7f; c2++; i+=c2;
                    c=fgetc(f); gzwrite(pakfile,&c,1);
                } else {
                    //raw
                    c2++;
                    while(c2>0){
                        c=fgetc(f); gzwrite(pakfile,&c,1);
                        i++;
                        c2--;
                    }
                }
            }
        }
        fclose(f);
        printf("OK\n");
    } else {
        printf("%s not found.\n",filename);
        return(1);
    }
    return(0);
}

int loadsnd(char *filename)
{
    unsigned int size=0;
    int f;
    byte *data;
    
    printf("SND: %s...",filename);
    f=open(filename,O_RDONLY);
    read(f,&size,2);
    data=(byte *)malloc(size+4);
    bcopy(&size,data,4);
    read(f,data+4,size);
    close(f);
    gzwrite(pakfile,data,size+2);
    free(data);
    printf("OK\n");
    return(0);
}

void readmaps(char* path)
{
    DIR *dh;
    struct dirent *de;
    unsigned char str[256];

    dh=opendir(path);
    de=readdir(dh);
    while(de!=NULL){
        if(!strncmp(de->d_name + strlen(de->d_name)-4,".map",4)){
            strcpy(str,path); strcat(str,de->d_name);
            loadmap(str);
        }
        de=readdir(dh);
    };
    closedir(dh);
}

int main(void)
{
    unlink("../stoneage.pak");
    //umask(0x124);
    pakfile=gzopen("../stoneage.pak","wb9");

    loadtga("tiles.tga");
    loadtga("sidebars.tga");
    loadtga("back.tga");
    loadtga("eyes.tga");
    loadtga("stoneage.tga");
    loadtga("main.tga");
    loadtga("light.tga");
    loadtga("pass.tga");
    loadtga("hiscore.tga");
    loadtga("endseq.tga");
    loadtga("fonts.tga");
    loadtga("fonts2.tga");
    loadtga("dragon.tga");
    loadtga("pause.tga");

    readmaps("../levels/");
    
    gzclose(pakfile);
}
