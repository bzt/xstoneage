#ifndef _XINIT_
#define _XINIT_ 1
#define TRUE 1
#define FALSE 0

#include <X11/Xlib.h>                                                                                                   
#include <X11/Xutil.h>                                                                                                  
#include <X11/Xos.h>                                                                                                    
#include <X11/Xatom.h>                                                                                                  
#include <X11/extensions/Xext.h>                                                                                        
//#include <X11/extensions/extutil.h>                                                                                     
//#include <X11/extensions/xf86vmode.h>                                                                                   

#define STANDARDPTR XC_left_ptr
#define WORKINGPTR XC_watch

#include "xstoneage.h"

#define memToVal(PTR,LEN) (\
(LEN) == 1 ? (unsigned long)(                 *( (byte *)(PTR))         ) :    \
(LEN) == 2 ? (unsigned long)(((unsigned long)(*( (byte *)(PTR))   ))<< 8)      \
                          + (                 *(((byte *)(PTR))+1)      ) :    \
(LEN) == 3 ? (unsigned long)(((unsigned long)(*( (byte *)(PTR))   ))<<16)      \
                          + (((unsigned long)(*(((byte *)(PTR))+1)))<< 8)      \
						  + (                 *(((byte *)(PTR))+2)      ) :    \
             (unsigned long)(((unsigned long)(*( (byte *)(PTR))   ))<<24)      \
						  + (((unsigned long)(*(((byte *)(PTR))+1)))<<16)      \
						  + (((unsigned long)(*(((byte *)(PTR))+2)))<< 8)      \
						  + (                 *(((byte *)(PTR))+3)      ) )

#define valToMem(VAL,PTR,LEN)  (\
(LEN) == 1 ? (*( (byte *)(PTR)   ) = ( VAL     ) ) : \
(LEN) == 2 ? (*( (byte *)(PTR)   ) = (((unsigned long)(VAL))>> 8),        \
              *(((byte *)(PTR))+1) = ( VAL     ) ) : \
(LEN) == 3 ? (*( (byte *)(PTR)   ) = (((unsigned long)(VAL))>>16),        \
              *(((byte *)(PTR))+1) = (((unsigned long)(VAL))>> 8),        \
              *(((byte *)(PTR))+2) = ( VAL     ) ) : \
             (*( (byte *)(PTR)   ) = (((unsigned long)(VAL))>>24),        \
              *(((byte *)(PTR))+1) = (((unsigned long)(VAL))>>16),        \
              *(((byte *)(PTR))+2) = (((unsigned long)(VAL))>> 8),        \
              *(((byte *)(PTR))+3) = ( VAL     ) ))

#define FADEIN 0
#define FADEOUT 1
#define FADEINOUT 2

#define MOTIF_WM_HINTS_DECORATIONS   (1L << 1)                                                                          
typedef struct                                                                                                          
{                                                                                                                       
 long flags;                                                                                                            
 long functions;                                                                                                        
 long decorations;                                                                                                      
 long input_mode;                                                                                                       
 long status;                                                                                                           
} _motif_wm_hints;                                                                                                      
_motif_wm_hints motif_wm_hints;                                                                                         

int scr;
Display *dis;
Window win;
XEvent report;
GC gc;
XGCValues gcv;
Colormap colormap;
Visual *vsl;
XWindowChanges newcfg;
XWindowAttributes attr;
//FILE *f;
int red_shift,blue_shift,green_shift;

unsigned char pal[768];

char title[256];
int titlelen;

void initdisplay(char* file, int x, int y);
void CopyImage(int mode, XImage *bg, int x1, int y1, XImage* ximage, int x2, int y2, int w, int h);
void fadeImage(int fadetype, int x, int y, XImage* ximage);
void subtitle(int x, int y, XImage* ximage, XImage* bg);
void dopause(int x, int y, int legend, XImage* pause, XImage* bg);
unsigned long int MkColor(int r, int g, int b);
unsigned long int MkColorTGA(int r, int g, int b);
void closedisplay();
void DestroyImage(XImage *ximage);
void fadeimage(int fadetype, int x, int y, XImage *ximage);

#endif
