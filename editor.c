#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/Intrinsic.h>
#include <X11/cursorfont.h>
#include <dirent.h>
#include <fcntl.h>
#include <zlib.h>
#include "xinit.h"
#include "xstoneage.h"

//other variables
extern int alt_pressed;
extern int mousebutton_pressed;
extern int mousex,mousey;

int currtile,tilewin,codewin,codeptr;

level levelmap;

extern XImage *tiles;
extern XImage *sidebars;
extern XImage *bgr;
extern XImage *fonts;

int fr,fg,fb;    //default foreground
int br,bg,bb;    //default background
int ifr,ifg,ifb; //default input foreground
int ibr,ibg,ibb; //default input background
int val3d;       //sharpness of 3d boxes

XWindowAttributes win_attr;

/**
 * create color variations
 */
long MkDarkColor(int r,int g,int b)
{
    r=br-val3d; g=bg-val3d; b=bb-val3d; if(r<0) r=0; if(g<0) g=0; if(b<0) b=0;
    if (red_shift>=0) r<<=red_shift; else r>>=-red_shift;
    if (green_shift>=0) g<<=green_shift; else g>>=-green_shift;
    if (blue_shift>=0) b<<=blue_shift; else b>>=-blue_shift;
    r&=vsl->red_mask;
    g&=vsl->green_mask;
    b&=vsl->blue_mask;
    return r+g+b;
}

long MkLightColor(int r,int g,int b)
{
    r=br+val3d; g=bg+val3d; b=bb+val3d; if(r>255) r=255; if(g>255) g=255; if(b>255) b=255;
    if (red_shift>=0) r<<=red_shift; else r>>=-red_shift;
    if (green_shift>=0) g<<=green_shift; else g>>=-green_shift;
    if (blue_shift>=0) b<<=blue_shift; else b>>=-blue_shift;
    r&=vsl->red_mask;
    g&=vsl->green_mask;
    b&=vsl->blue_mask;
    return r+g+b;
}

/**
 * draw a button
 */
void button(int pressed, int x, int y, int width, int height){
    int dco,co,lco,i,r,g,b;

    dco=MkDarkColor(r,g,b);
    co=MkColor(br,bg,bb);
    lco=MkLightColor(r,g,b);
    if(pressed==TRUE){i=dco; dco=lco; lco=i;}
    XSetForeground(dis, gc, lco);
    XDrawRectangle(dis, win, gc, x+1, y, width-3, 1);
    XDrawRectangle(dis, win, gc, x, y+1, 1, height-3);
    XSetForeground(dis, gc, dco);
    XDrawRectangle(dis, win, gc, x+1, y+height-2, width-3, 1);
    XDrawRectangle(dis, win, gc, x+width-2, y+1, 1, height-3);
}

/**
 * change cursor
 */
void SetCursor(Cursor cursor)
{
    XSetWindowAttributes swa;
    swa.cursor=XCreateFontCursor(dis,cursor);
    XChangeWindowAttributes(dis, win, CWCursor, &swa);
}

/**
 * draw editor window
 */
void exposehandlered(){
    int i,j;
    char dummy[8];
    
    byte tmp;
    XGetWindowAttributes(dis, win, &win_attr);

    XSetForeground(dis, gc, MkColor(bg,bg,bg));
    XFillRectangle(dis, win, gc, 640, 0, 100, 400);

//menu
    XSetForeground(dis, gc, MkColor(fr,fg,fb));
    XSetFont(dis,gc,XLoadFont(dis,"-*-*-*-*-*-*-14-*-*-*-*-*-*-*"));
    XDrawString(dis,win,gc,660,35,"<- Back",7);
    XDrawString(dis,win,gc,660,75,"New",3);
    XDrawString(dis,win,gc,660,105,"Save",4);
    XDrawString(dis,win,gc,660,135,"Load",4);
    XDrawString(dis,win,gc,660,165,"Backgrnd",8);
    XDrawString(dis,win,gc,660,195,"Tile Set",8);
    XDrawString(dis,win,gc,700,225,"...",3);
    XDrawString(dis,win,gc,660,275,"Time +/=",8);
    XDrawString(dis,win,gc,650,315,"Code:",5);
    XDrawString(dis,win,gc,660,335,levelmap.code,strlen(levelmap.code));
    XDrawString(dis,win,gc,650,355,"Next level:",11);
    XDrawString(dis,win,gc,660,375,levelmap.nextlink,strlen(levelmap.nextlink));

    button(FALSE,640,0,win_attr.width-640,win_attr.height);

    button(FALSE,650,20,win_attr.width-660,20);
    button(FALSE,650,60,win_attr.width-660,20);
    button(FALSE,650,90,win_attr.width-660,20);
    button(FALSE,650,120,win_attr.width-660,20);
    button(FALSE,650,150,win_attr.width-660,20);
    button(FALSE,650,180,win_attr.width-660,20);
    button(TRUE,650,210,36,36);
    button(tilewin,690,210,win_attr.width-700,20);
    button(FALSE,650,260,win_attr.width-660,20);
    button(codewin%2,650,320,win_attr.width-660,20);
    button(codewin/2,650,360,win_attr.width-660,20);
    
    XPutImage(dis, win, gc, tiles, (currtile/3)*32, (currtile%3)*32+levelmap.tileset*96,652,212,32,32);

    //scene
    for(i=0;i<11;i++){
        for(j=0;j<20;j++){
            tmp=levelmap.map[i*20+j];
            if(tmp==0xff) {
                XSetForeground(dis, gc, MkColor(255,255,255));
                XFillRectangle(dis, win, gc, j*32, i*32+31, 31, 1);
                XFillRectangle(dis, win, gc, j*32+31, i*32, 1, 31);
                XPutImage(dis, win, gc, bgr, levelmap.tileset*640+j*32,levelmap.background*352+i*32,j*32,i*32,31,31);
            } else {
                XPutImage(dis, win, gc, tiles, (tmp/3)*32, (tmp%3)*32+levelmap.tileset*96,j*32,i*32,32,32);
            }
        }
    }

    //side bar and time to solve puzzle
    XPutImage(dis, win, gc, sidebars, 0, levelmap.sidebar*45,0,win_attr.height-48,640,45);
    XSetForeground(dis, gc, MkColor(255,255,255));
    sprintf(dummy,"%d",levelmap.soltime);
    for(i=0;i<strlen(dummy);i++){
        XPutImage(dis, win, gc, fonts, (dummy[i]-'0')*16, 34,95-(strlen(dummy)-i)*16,368,16,16);
    }

    if(tilewin){
        XPutImage(dis, win, gc, tiles, 0, levelmap.tileset*96,(640-352)/2,140,352,96);
        button(TRUE,(640-352)/2-2,138,356,100);
        button(FALSE,(640-352)/2-4,136,360,104);
    }
    XSync(dis,FALSE);
}

/**
 * read a level map
 */
void readmap(char* filename){
    char fn[256];
    int f;

    if(filename==NULL || strlen(filename)==0){
        if(strlen(levelmap.code)<6){
            printf("No legal code given, nothing to be load.\n"); fflush(stdout); return;
        }else{
            strcpy(fn,getenv("HOME"));
            strcat(fn,"/.xstoneage/");
            strcat(fn,levelmap.code);
            strcat(fn,".map");
            filename=fn;
        }
    }
    printf("Loading: %s...",filename);
    fflush(stdout);

    f=open(filename,O_RDONLY);
    if(f<3){
        printf(" not found.\n"); fflush(stdout);
    } else {
        read(f,&levelmap,sizeof(level));
        printf(" done.\n"); fflush(stdout);
    }
    close(f);
}

/**
 * save a level map
 */
void savemap(char* filename){
    char fn[256];
    int f,sp=0,ep=0;

    for(f=0;f<220;f++) {
        if(levelmap.map[f]==12) sp++;
        if(levelmap.map[f]==13) ep++;
    }

    if(filename==NULL || strlen(filename)==0){
        if(strlen(levelmap.code)<6){
            printf("No legal code given, NOT SAVED.\n"); fflush(stdout); return;
        } else if(sp!=1||ep!=1) {
            printf("Must have one start and exit blocks, NOT SAVED.\n"); fflush(stdout); return;
        } else {
            strcpy(fn,getenv("HOME"));
            strcat(fn,"/.xstoneage/");
            mkdir((const char*)&fn,RIGHTS(7,5,5));
            strcat(fn,levelmap.code);
            strcat(fn,".map");
            filename=fn;
        }
    }
    SetCursor(WORKINGPTR); XFlush(dis);
    printf("Saving: %s\n",filename);
    fflush(stdout);
    
    f=open(filename,O_CREAT|O_WRONLY,RIGHTS(6,4,4));
    write(f,&levelmap,sizeof(level));
    close(f);
    sleep(1);
    SetCursor(STANDARDPTR);
    XFlush(dis);
}

/**
 * create a new, empty level map
 */
void newmap(){
    int f;
    levelmap.code[0]=0;
    levelmap.code[1]=0;
    levelmap.code[2]=0;
    levelmap.code[3]=0;
    levelmap.code[4]=0;
    levelmap.code[5]=0;
    levelmap.code[6]=0;
    if(levelmap.nextlink!=0) {
        strcpy(levelmap.code,levelmap.nextlink);
        levelmap.nextlink[0]=0;
    }
    levelmap.tileset=0;
    levelmap.background=0;
    levelmap.sidebar=0;
    levelmap.soltime=100;
    for(f=0;f<220;f++)
        levelmap.map[f]=0xff;
}

void editor()
{
    int f;
    char c;

    //default colors
    br=bg=bb=150;
    fr=fg=fb=0;
    ibr=ibg=ibb=255;
    ifr=ifg=ifb=0;
    val3d=63;
    
    //start with empty map
    newmap();
    levelmap.nextlink[0]='S';
    levelmap.nextlink[1]='R';
    levelmap.nextlink[2]='T';
    levelmap.nextlink[3]='L';
    levelmap.nextlink[4]='E';
    levelmap.nextlink[5]='V';
    levelmap.nextlink[6]=0;
    tilewin=0;

    exposehandlered();
    
    //menu comes here
    while (1)  {
        report.type=None;
        XNextEvent(dis, &report);
        switch  (report.type) {

        case ButtonPress:
#if DEBUG==TRUE
            printf("Mouse button pressed at %d %d.\n",report.xbutton.x,report.xbutton.y);
#endif
            mousebutton_pressed=TRUE;
            mousex=report.xbutton.x;
            mousey=report.xbutton.y;
            XGetWindowAttributes(dis, win, &win_attr);
            if(tilewin==0){
                if (mousex<=640 && mousey<=11*32) {
                    f=(mousey/32*20)+mousex/32;
                    if(report.xbutton.button==1) levelmap.map[f]=currtile;
                    if(report.xbutton.button==3) levelmap.map[f]=0xff;
                    exposehandlered();
                }
                if (mousex<=640 && mousey>11*32) {
                    if(report.xbutton.button==1 && levelmap.sidebar<(sidebars->height/45-1)) levelmap.sidebar++;
                    if(report.xbutton.button==3 && levelmap.sidebar>0) levelmap.sidebar--;
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=20 && mousey<=40) {
                    button(TRUE,650,20,win_attr.width-660,20);
                    while(report.type!=ButtonRelease){XNextEvent(dis, &report);}
                    return;
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=60 && mousey<=80) {
                    button(TRUE,650,60,win_attr.width-660,20);
                    newmap();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=90 && mousey<=110) {
                    button(TRUE,650,90,win_attr.width-660,20);
                    savemap(NULL);
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=120 && mousey<=140) {
                    button(TRUE,650,120,win_attr.width-660,20);
                    readmap(NULL);
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=150 && mousey<=170) {
                    button(TRUE,650,150,win_attr.width-660,20);
                    if(report.xbutton.button==1 && levelmap.background<(bgr->height/352-1)) levelmap.background++;
                    if(report.xbutton.button==3 && levelmap.background>0) levelmap.background--;
                    while(report.type!=ButtonRelease){XNextEvent(dis, &report);}
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=180 && mousey<=200) {
                    button(TRUE,650,180,win_attr.width-660,20);
                    if(report.xbutton.button==1 && levelmap.tileset<(tiles->height/96-1)) levelmap.tileset++;
                    if(report.xbutton.button==3 && levelmap.tileset>0) levelmap.tileset--;
                }
                if (mousex>=650 && mousex<=686 && mousey>=210 && mousey<=246) {
                    if(report.xbutton.button==1 && currtile<32) currtile++;
                    if(report.xbutton.button==3 && currtile>0) currtile--;
                    exposehandlered();
                }
                if (report.xbutton.button==2 || 
                    (mousex>=690 && mousex<=win_attr.width-10 && mousey>=210 && mousey<=230)) {
                        tilewin=1;
                        exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=260 && mousey<=280) {
                    button(TRUE,650,260,win_attr.width-660,20);
                    if(report.xbutton.button==1)
                        if(levelmap.soltime<4200) levelmap.soltime+=10; else levelmap.soltime=4200;
                    if(report.xbutton.button==3)
                        if(levelmap.soltime>10) levelmap.soltime-=10; else levelmap.soltime=10;
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=310 && mousey<=340) {
                    codewin=1; codeptr=strlen(levelmap.code);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=350 && mousey<=380) {
                    codewin=2; codeptr=strlen(levelmap.nextlink);
                    exposehandlered();
                }
        
                if(report.xbutton.button==5 && currtile<32) {currtile++; exposehandlered();}
                if(report.xbutton.button==4 && currtile>0) {currtile--; exposehandlered();}
            } else {
                if(mousex>=(640-352)/2 && mousex<=(640+352)/2 && mousey>=140 && mousey<=236) {
                    currtile=(mousey-140)/32+(mousex-(640-352)/2)/32*3;
                }
                tilewin=0; exposehandlered();
            }
            break;

        case ButtonRelease:
#if DEBUG==TRUE
            printf("Mouse button released at %d %d.\n",report.xbutton.x,report.xbutton.y);
#endif
            mousebutton_pressed=FALSE;
            XGetWindowAttributes(dis, win, &win_attr);
            if(tilewin==0){
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=60 && mousey<=80) {
                    button(FALSE,650,60,win_attr.width-660,20);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=90 && mousey<=110) {
                    button(FALSE,650,90,win_attr.width-660,20);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=120 && mousey<=140) {
                    button(FALSE,650,120,win_attr.width-660,20);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=150 && mousey<=170) {
                    button(FALSE,650,150,win_attr.width-660,20);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=180 && mousey<=200) {
                    button(FALSE,650,180,win_attr.width-660,20);
                    exposehandlered();
                }
                if (mousex>=650 && mousex<=win_attr.width-10 && mousey>=260 && mousey<=280) {
                    button(FALSE,650,260,win_attr.width-660,20);
                    exposehandlered();
                }
            }
            break;

        case ResizeRequest:   
#if DEBUG==TRUE
            printf("I have been resized.\n");
#endif
            XClearWindow(dis,win);
            exposehandlered();
            break;

        case Expose:
#if DEBUG==TRUE
            printf("I have been exposed.\n");
#endif
            exposehandlered();
            break;

        case KeyPress:
#if DEBUG==TRUE
            printf("keypress: %X\n",XLookupKeysym(&report.xkey, 0));
#endif
            /*Close the editor if Escape or F5 is pressed.*/
            if (XLookupKeysym(&report.xkey, 0) == XK_Escape) {
                    if(tilewin==1){
                        tilewin=0; exposehandlered();
                    } else if(codewin>0){
                        codewin=0; exposehandlered();
                    } else return;
            }
            if (XLookupKeysym(&report.xkey, 0) == XK_F5) {
                return;
            }
            //level code input
            if(codewin>0){
                f=1; c=0;
                if(XLookupKeysym(&report.xkey,0) == XK_Return)
                    if(codeptr==6) {codewin=0;f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_BackSpace)
                    if(codeptr<=0){
                        codeptr=0;
                    } else { 
                        codeptr--;
                        if(codewin==1) levelmap.code[codeptr]=0; else levelmap.nextlink[codeptr]=0;
                        f=0;
                    }
                if(XLookupKeysym(&report.xkey,0) == XK_a) if(codeptr<6){ c='A'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_b) if(codeptr<6){ c='B'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_c) if(codeptr<6){ c='C'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_d) if(codeptr<6){ c='D'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_e) if(codeptr<6){ c='E'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_f) if(codeptr<6){ c='F'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_g) if(codeptr<6){ c='G'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_h) if(codeptr<6){ c='H'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_i) if(codeptr<6){ c='I'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_j) if(codeptr<6){ c='J'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_k) if(codeptr<6){ c='K'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_l) if(codeptr<6){ c='L'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_m) if(codeptr<6){ c='M'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_n) if(codeptr<6){ c='N'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_o) if(codeptr<6){ c='O'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_p) if(codeptr<6){ c='P'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_q) if(codeptr<6){ c='Q'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_r) if(codeptr<6){ c='R'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_s) if(codeptr<6){ c='S'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_t) if(codeptr<6){ c='T'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_u) if(codeptr<6){ c='U'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_v) if(codeptr<6){ c='V'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_w) if(codeptr<6){ c='W'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_x) if(codeptr<6){ c='X'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_y) if(codeptr<6){ c='Y'; f=0;}
                if(XLookupKeysym(&report.xkey,0) == XK_z) if(codeptr<6){ c='Z'; f=0;}
                if(f){
                    printf("\007"); fflush(stdout);
                }
                if(c){
                    if(codewin==1){
                        levelmap.code[codeptr]=c;
                        levelmap.code[codeptr+1]=0;
                    }else{
                        levelmap.nextlink[codeptr]=c;
                        levelmap.nextlink[codeptr+1]=0;
                    }
                    codeptr++;
                }
                exposehandlered();
            }
            break;
        case KeyRelease:
#if DEBUG==TRUE
            printf("keyrelease: %X\n",XLookupKeysym(&report.xkey, 0));
#endif
            if (XLookupKeysym(&report.xkey, 0) == XK_Alt_R ||
                XLookupKeysym(&report.xkey, 0) == XK_Alt_L) {
                    alt_pressed=FALSE;
            }
            break;

        default:
            break;
        }
    }
    // menu end, this code should be never reached
    return;
}

