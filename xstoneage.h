#ifndef _XSTONEAGE_
#define _XSTONEAGE_ 1

typedef unsigned char  byte;      /* unsigned byte type */

#include <X11/Xlib.h>
#include "stoneage.h"

#define DEBUG FALSE

#define RIGHTS(u,g,o) (u*8+g)*8+o

//levels
int numlevels;
level levels[256];

//setup
int volume;
int difficult;

//prototypes
void setup();
void byez();
void loadcfgfile(void);
void savecfgfile(void);
void loadscorefile(void);
void savescorefile(void);
void displayhiscore(void);
XImage *CreateEmptyImage();
int readlevel();
void dothegame();

typedef struct {
    unsigned long score;
    unsigned int level;
    char name[12];
} hscoretype;

#endif
