#!/bin/bash

maps="*.map"
#lev="SRTLEV"
lev="SECRET"	#secret levels fallbacks to SRTLEV
end=0
level=1
while [ $end -eq 0 ]
do
	nextlev=`cat $lev.map | head -1 | cut -b 7-13 2>/dev/null`
	echo -e "Checking $level. level: $lev (-$nextlev)"
	if [ "$nextlev" == "ENDSEQ" ]
	then
		echo "End."
		end=1
	else
		if [ -f $nextlev.map ]
		then
			for j in $maps
			do
				map=`echo $j | cut -d ',' -f 1`
				cnt=`echo $j | cut -d ',' -f 2`
				if [ "$cnt" == "$map" ]; then cnt=0; fi
				if [ "$map" == "$nextlev.map" ]
				then
					cnt=$[$cnt+1]
					maps=`echo $maps | sed s/$j/$map\,$cnt/g`
				fi
			done
			lev=$nextlev
			level=$[$level+1]
		else
			echo "No such map: $nextlev in $lev.map"
			end=1
		fi
	fi
done
echo ""
echo "*** SUMMARY ***"
echo "Last level: $lev"
echo -n "Unrefered levels:"
for i in $maps
do
	map=`echo $i | cut -d ',' -f 1`
	cnt=`echo $i | cut -d ',' -f 2`
	if [ "$cnt" == "$map" ]; then cnt=0; fi
	if [ $cnt -eq 0 ]; then	echo -n " $map"; fi
done
echo ""
echo -n "More than once referred:"
for i in $maps
do
	map=`echo $i | cut -d ',' -f 1`
	cnt=`echo $i | cut -d ',' -f 2`
	if [ "$cnt" == "$map" ]; then cnt=0; fi
	if [ $cnt -gt 1 ]; then	echo -n " $map"; fi
done
echo ""
